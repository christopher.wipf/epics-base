From: Michael Davidsaver <mdavidsaver@gmail.com>
Date: Fri, 8 Jul 2016 16:20:35 -0400
Subject: bswap16/32 as functions

Turn bswap16() and bswap32() into functions
to avoid evaluating the argument more than once.
---
 src/libCom/osi/os/RTEMS/epicsMMIO.h      | 20 ++++++++++++++++++++
 src/libCom/osi/os/default/epicsMMIODef.h | 24 ++++++++++++++++--------
 src/libCom/osi/os/vxWorks/epicsMMIO.h    | 27 ++++++++++++++++++---------
 3 files changed, 54 insertions(+), 17 deletions(-)

diff --git a/src/libCom/osi/os/RTEMS/epicsMMIO.h b/src/libCom/osi/os/RTEMS/epicsMMIO.h
index 7db6670..293886b 100644
--- a/src/libCom/osi/os/RTEMS/epicsMMIO.h
+++ b/src/libCom/osi/os/RTEMS/epicsMMIO.h
@@ -12,6 +12,8 @@
 #define EPICSMMIO_H
 
 #include <epicsEndian.h>
+#include <epicsTypes.h>
+#include <compilerSpecific.h>
 
 #if defined(_ARCH_PPC) || defined(__PPC__) || defined(__PPC)
 #  include <libcpu/io.h>
@@ -39,6 +41,24 @@
 #  define nat_iowrite16 be_iowrite16
 #  define nat_iowrite32 be_iowrite32
 
+static EPICS_ALWAYS_INLINE
+epicsUInt16
+bswap16(epicsUInt16 value)
+{
+    return (((epicsUInt16)(value) & 0x00ff) << 8)    |
+           (((epicsUInt16)(value) & 0xff00) >> 8);
+}
+
+static EPICS_ALWAYS_INLINE
+epicsUInt32
+bswap32(epicsUInt32 value)
+{
+    return (((epicsUInt32)(value) & 0x000000ff) << 24)   |
+           (((epicsUInt32)(value) & 0x0000ff00) << 8)    |
+           (((epicsUInt32)(value) & 0x00ff0000) >> 8)    |
+           (((epicsUInt32)(value) & 0xff000000) >> 24);
+}
+
 #elif defined(i386) || defined(__i386__) || defined(__i386) || defined(__m68k__)
 
 /* X86 does not need special handling for read/write width.
diff --git a/src/libCom/osi/os/default/epicsMMIODef.h b/src/libCom/osi/os/default/epicsMMIODef.h
index 087f880..110e3a9 100644
--- a/src/libCom/osi/os/default/epicsMMIODef.h
+++ b/src/libCom/osi/os/default/epicsMMIODef.h
@@ -89,15 +89,23 @@ nat_iowrite32(volatile void* addr, epicsUInt32 val)
  *@{
  */
 
-#define bswap16(value) ((epicsUInt16) (  \
-        (((epicsUInt16)(value) & 0x00ff) << 8)    |       \
-        (((epicsUInt16)(value) & 0xff00) >> 8)))
+static EPICS_ALWAYS_INLINE
+epicsUInt16
+bswap16(epicsUInt16 value)
+{
+    return (((epicsUInt16)(value) & 0x00ff) << 8)    |
+           (((epicsUInt16)(value) & 0xff00) >> 8);
+}
 
-#define bswap32(value) (  \
-        (((epicsUInt32)(value) & 0x000000ff) << 24)   |                \
-        (((epicsUInt32)(value) & 0x0000ff00) << 8)    |                \
-        (((epicsUInt32)(value) & 0x00ff0000) >> 8)    |                \
-        (((epicsUInt32)(value) & 0xff000000) >> 24))
+static EPICS_ALWAYS_INLINE
+epicsUInt32
+bswap32(epicsUInt32 value)
+{
+    return (((epicsUInt32)(value) & 0x000000ff) << 24)   |
+           (((epicsUInt32)(value) & 0x0000ff00) << 8)    |
+           (((epicsUInt32)(value) & 0x00ff0000) >> 8)    |
+           (((epicsUInt32)(value) & 0xff000000) >> 24);
+}
 
 #  define be_ioread16(A)    nat_ioread16(A)
 #  define be_ioread32(A)    nat_ioread32(A)
diff --git a/src/libCom/osi/os/vxWorks/epicsMMIO.h b/src/libCom/osi/os/vxWorks/epicsMMIO.h
index 06301fd..25bd040 100644
--- a/src/libCom/osi/os/vxWorks/epicsMMIO.h
+++ b/src/libCom/osi/os/vxWorks/epicsMMIO.h
@@ -37,6 +37,7 @@
 
 #include  <epicsTypes.h>                        /* EPICS Common Type Definitions                  */
 #include  <epicsEndian.h>                       /* EPICS Byte Order Definitions                   */
+#include  <compilerSpecific.h>
 
 /*=====================
  * vxAtomicLib.h (which defines the memory barrier macros)
@@ -49,15 +50,23 @@
 #  include  <vxAtomicLib.h>
 #endif
 
-#define bswap16(value) ((epicsUInt16) (  \
-        (((epicsUInt16)(value) & 0x00ff) << 8)    |       \
-        (((epicsUInt16)(value) & 0xff00) >> 8)))
-
-#define bswap32(value) (  \
-        (((epicsUInt32)(value) & 0x000000ff) << 24)   |                \
-        (((epicsUInt32)(value) & 0x0000ff00) << 8)    |                \
-        (((epicsUInt32)(value) & 0x00ff0000) >> 8)    |                \
-        (((epicsUInt32)(value) & 0xff000000) >> 24))
+static EPICS_ALWAYS_INLINE
+epicsUInt16
+bswap16(epicsUInt16 value)
+{
+    return (((epicsUInt16)(value) & 0x00ff) << 8)    |
+           (((epicsUInt16)(value) & 0xff00) >> 8);
+}
+
+static EPICS_ALWAYS_INLINE
+epicsUInt32
+bswap32(epicsUInt32 value)
+{
+    return (((epicsUInt32)(value) & 0x000000ff) << 24)   |
+           (((epicsUInt32)(value) & 0x0000ff00) << 8)    |
+           (((epicsUInt32)(value) & 0x00ff0000) >> 8)    |
+           (((epicsUInt32)(value) & 0xff000000) >> 24);
+}
 
 #if EPICS_BYTE_ORDER == EPICS_ENDIAN_BIG
 #  define be16_to_cpu(X) (epicsUInt16)(X)
